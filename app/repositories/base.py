from typing import Any
from typing import Optional
from typing import Type
from typing import TypeVar

from sqlalchemy import Result
from sqlalchemy import select
from sqlalchemy import update
from sqlalchemy.ext.asyncio import AsyncSession

from app.models.base import AbstractBaseModel

Entity = TypeVar("Entity", bound=AbstractBaseModel)


class BaseRepository:
    def __init__(self, session: AsyncSession):
        self.session = session

    async def _get_all(self, model: Type[Entity]):
        """
        The _get_all function is a helper function that returns all of the rows in a table.
        It takes one argument, model, which is the type of entity to be returned.
        The result variable stores the results from executing an SQLAlchemy select statement on model.
        The return value is all of the scalars (columns) from each row in result.

        :param self: Access the attributes and methods of the class
        :param model: Type[Entity]: Specify the model that we want to query
        :return: All the rows in a table
        """
        result = await self.session.execute(select(model))
        return result.scalars().all()

    async def _get(self, model: Type[Entity], param) -> Optional[Entity]:
        """
        The _get function is a helper function that will be used by the get_by_id and get_by functions.
        It takes in a model, which is the table we are querying from, and param, which is either an id or
        a dictionary of parameters to filter by. It then returns either one entity if it finds one or None if it doesn't.

        :param self: Represent the instance of the class
        :param model: Type[Entity]: Specify the type of model we are looking for
        :param param: Filter the query by id
        :return: The first row of the result set
        """
        res = await self.session.execute(select(model).filter_by(id=param))
        return res.scalar()

    async def _create(self, entity) -> Entity:
        """
        The _create function is a helper function that creates an entity and adds it to the database.
        It then flushes the session, which means that all pending changes are written to the database.
        Finally, it returns the created entity.

        :param self: Represent the instance of the class
        :param entity: Create a new entity in the database
        :return: The entity that was just created
        """
        self.session.add(entity)
        await self.session.flush()
        return entity

    async def _update(self, model, instance, values: dict) -> Result[Any]:
        """
        The _update function is a helper function that updates the values of an instance.
        It takes in three arguments: model, instance, and values. The model argument is the table
        that we are updating from (i.e., User). The instance argument is the row that we are updating
        (i.e., user_instance). And finally, the values argument contains all of our new data to update
        the row with (i.e., {'username': 'new_username', 'password': 'new_password'}).

        :param self: Make the method a bound method, which means that it can be called on an instance of the class
        :param model: Specify the table to update
        :param instance: Get the id of the instance to update
        :param values: dict: Pass in the values that we want to update
        :return: A result object
        """
        res = await self.session.execute(
            (update(model).where(model.id == instance.id).values(**values))
        )
        await self.session.flush()
        return res

    async def _delete(self, instance) -> None:
        """
        The _delete function is a helper function that deletes an instance of the model.
        It takes in one parameter, which is the instance to be deleted. It then calls delete on
        the session and flushes it.

        :param self: Represent the instance of the class
        :param instance: Specify the instance of the object that we want to delete
        :return: None
        """
        res = await self.session.delete(instance)
        await self.session.flush()
        return res
