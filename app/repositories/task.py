from typing import Optional
from typing import Tuple
from uuid import UUID

from sqlalchemy import and_
from sqlalchemy import Result
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models.task import Task
from app.repositories.base import BaseRepository
from app.schemas.task import CreateTaskSchema


class TaskRepository(BaseRepository):
    def __init__(self, session: AsyncSession):
        super().__init__(session)

    async def create_new_task(self, values: CreateTaskSchema) -> Task:
        """
        The create_new_task function creates a new task in the database.

        :param self: Access the current object
        :param values: CreateTaskSchema: Create a new task
        :return: A task object
        """
        new_task = Task(**values.model_dump())
        return await self._create(new_task)

    async def get_tasks(self, todo_list_ids: Tuple[UUID]):
        """
        The get_tasks function returns all tasks in the database that are associated with a todo list.

        :param self: Refer to the current object, and is used in method definitions inside a class
        :param todo_list_ids: Tuple[UUID]: Specify the type of the parameter
        :return: A list of task objects
        """
        query = select(Task).filter(Task.todo_list_id.in_(todo_list_ids))
        res = await self.session.execute(query)
        return res.scalars().all()

    async def get_user_task(
        self, task_id: UUID, todo_list_ids: Tuple[UUID]
    ) -> Optional[Task]:
        """
        The get_user_task function returns a task that belongs to the user.

        :param self: Access the current object
        :param task_id: UUID: Specify the id of the task we want to retrieve
        :param todo_list_ids: Tuple[UUID]: Filter the query to only
        :return: A single task, or none if no task is found
        """
        query = select(Task).filter(
            and_(Task.id == task_id, Task.todo_list_id.in_(todo_list_ids))
        )
        res = await self.session.execute(query)
        return res.scalar_one_or_none()

    async def updating_task_by_user(self, instance: Task, **kwargs) -> Result[Task]:
        """
        The updating_task_by_user function updates a task by user.

        :param self: Make the method a bound method
        :param instance: Task: Pass the instance of the task that is being updated
        :param **kwargs: Pass a variable number of keyword arguments to the function
        :return: A result[task] object
        """
        return await self._update(Task, instance, kwargs)

    async def deleting_task_by_user(self, task: Task) -> None:
        """
        The deleting_task_by_user function is a coroutine that deletes the task from the database.

        :param self: Represent the instance of the class
        :param task: Task: Pass in the task object that is to be deleted
        :return: None
        """
        return await self._delete(task)
