from typing import Optional
from uuid import UUID

from sqlalchemy import Result
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models.todo_list import ToDoList
from app.repositories.base import BaseRepository


class TodoListRepository(BaseRepository):
    def __init__(self, session: AsyncSession):
        super().__init__(session)

    async def create_todo(self, new_list: ToDoList) -> ToDoList:
        """
        The create_todo function creates a new todo list.

        :param self: Refer to the current instance of a class
        :param new_list: ToDoList: Pass in the new list that we want to create
        :return: A todolist object
        """
        return await self._create(new_list)

    async def get_all_user_lists(self, user_id: UUID):
        """
        The get_all_user_lists function returns all of the lists that a user has created.


        :param self: Allow an instance of the class to access its own attributes and methods
        :param user_id: UUID: Filter the query
        :return: A list of all the to-do lists that belong to a specific user
        """
        query = select(ToDoList).filter_by(user_id=user_id)
        res = await self.session.execute(query)
        return res.scalars().all()

    async def get_list_by_id(self, list_id: UUID) -> Optional[ToDoList]:
        """
        The get_list_by_id function is a helper function that returns the ToDoList object with the given id.

        :param self: Refer to the instance of the class
        :param list_id: UUID: Get the list by id
        :return: A todolist object
        """
        return await self._get(ToDoList, list_id)

    async def update_list_by_user(
        self, instance: ToDoList, **kwargs
    ) -> Result[ToDoList]:
        """
        The update_list_by_user function updates a ToDoList instance in the database.

        :param self: Refer to the class itself
        :param instance: ToDoList: Pass the instance of the object to be updated
        :param **kwargs: Pass a dictionary of keyword arguments to the function
        :return: A result[todolist]
        """
        return await self._update(ToDoList, instance, kwargs)

    async def deleting_todo(self, todo_list: ToDoList) -> None:
        """
        The deleting_todo function is a coroutine that deletes the todo_list object from the database.


        :param self: Represent the instance of a class
        :param todo_list: ToDoList: Pass in the todo list object that we want to delete
        :return: None
        """
        return await self._delete(todo_list)
