from typing import List
from typing import Optional
from uuid import UUID

from sqlalchemy import Result
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models.user import User
from app.repositories.base import BaseRepository


class UserRepository(BaseRepository):
    def __init__(self, session: AsyncSession):
        super().__init__(session)

    async def get_all_users(self) -> List[User]:
        """
        The get_all_users function returns a list of all users in the database.

        :param self: Refer to the object on which this method is being called
        :return: A list of user objects
        """
        return await self._get_all(User)

    async def get_user_by_id(self, user_id: UUID) -> Optional[User]:
        """
        The get_user_by_id function returns a user object given the user's id.

        :param self: Represent the instance of a class
        :param user_id: UUID: Specify the type of parameter that is being passed into the function
        :return: A user object
        """
        return await self._get(User, user_id)

    async def create_user(self, user: User) -> User:
        """
        The create_user function creates a new user in the database.

        :param self: Refer to the current instance of a class
        :param user: User: Pass in the user object that we want to create
        :return: The user object that was created
        """
        return await self._create(user)

    async def updated_user(self, instance: User, **kwargs) -> Result[User]:
        """
        The updated_user function is a coroutine that takes in an instance of the User class and
        a keyword argument dictionary. It returns a Result object containing either an updated user or
        an error message.

        :param self: Access the class instance
        :param instance: User: Pass the instance of the user that is being updated
        :param **kwargs: Pass a variable number of keyword arguments to the function
        :return: A result[user]
        """
        return await self._update(User, instance, kwargs)

    async def delete_user(self, user: User) -> None:
        """
        The delete_user function deletes a user from the database.

        :param self: Represent the instance of the class
        :param user: User: Specify the user to be deleted
        :return: None
        """
        return await self._delete(user)

    async def get_user_by_username(self, username: str) -> Optional[User]:
        """
        The get_user_by_username function returns a user object given a username.

        :param self: Refer to the current object
        :param username: str: Specify the username of the user we want to get
        :return: A single user object, or none if no user is found
        """
        query = select(User).where(User.username == username).fetch(count=1)
        res = await self.session.execute(query)
        return res.scalar()
