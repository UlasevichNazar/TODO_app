from fastapi import APIRouter
from fastapi import Depends
from fastapi.security import OAuth2PasswordRequestForm

from app.schemas.login import Token
from app.services.auth import AuthService

login_router = APIRouter()
def new_func():
    pass
@login_router.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    """
    The login_for_access_token function is a POST endpoint that takes in the user's username and password,
    and returns an access token if the credentials are valid. The access token can then be used to authenticate
    the user for future requests.

    :param form_data: OAuth2PasswordRequestForm: Validate the request body
    :return: A token_type of bearer
    """
    access_token = await AuthService.authenticate_user(form_data)
    return {"access_token": access_token, "token_type": "bearer"}


