from logging import getLogger
from typing import List
from typing import Union
from uuid import UUID

from fastapi import APIRouter
from fastapi import Depends
from fastapi.exceptions import RequestValidationError
from fastapi.responses import UJSONResponse
from sqlalchemy.exc import IntegrityError

from app.exceptions.exceptions import EmptyParametersException
from app.exceptions.exceptions import ObjectNotFoundException
from app.exceptions.exceptions import PermissionDeniedException
from app.models.user import User
from app.permissions.user import UserPermissionsService
from app.schemas.user import CreateUserSchema
from app.schemas.user import ShowAdminSchema
from app.schemas.user import ShowUserSchema
from app.schemas.user import UpdateUserRequestSchema
from app.services.auth import AuthService
from app.services.user import UserService
from app.api.v1.todo_list import some_changes

logger = getLogger(__name__)

user_router = APIRouter()


@user_router.get("/", response_model=Union[List[ShowUserSchema], ShowUserSchema])
async def get_all_users_from_db(
        current_user: User = Depends(AuthService.get_current_user_from_token),
):
    """
    The get_all_users_from_db function is a GET request that returns all users in the database.
    It requires an authorization token to be passed as a header,
    and it will return an error if the user does not have permission to access this endpoint.

    :param current_user: User: Get the current user from the token
    :return: A list of all users in the database
    """
    try:
        return await UserService.get_all_users(current_user)
    except IntegrityError:
        raise


@user_router.get("/{user_id}", response_model=ShowUserSchema)
async def get_user_by_id(
        user_id: UUID, current_user: User = Depends(AuthService.get_current_user_from_token)
):
    """
    The get_user_by_id function is used to retrieve a user by their id.

    :param user_id: UUID: Specify the type of the parameter
    :param current_user: User: Get the current user from the token
    :return: A user object
    """
    user = await UserService.get_user(user_id)
    if user is None:
        raise ObjectNotFoundException(detail=f"User with id - {user_id} is not found.")
    if not await UserPermissionsService.check_user_permissions(
            target_user=user, current_user=current_user
    ):
        raise PermissionDeniedException()
    return user


def func():
    pass


@user_router.post("/", response_model=ShowUserSchema)
async def create_user(body: CreateUserSchema):
    """
    The create_user function creates a new user in the database.
        ---
        post:
          description: Creates a new user in the database.

    :param body: CreateUserSchema: Validate the request body
    :return: A user object
    """
    try:
        return await UserService.create_new_user(body)
    except IntegrityError:
        raise
    except RequestValidationError:
        raise


@user_router.post("/admin", response_model=ShowAdminSchema)
async def create_admin_user(body: CreateUserSchema):
    """
    The create_admin_user function creates a new admin user.
        ---
        post:
          description: Create an admin user.

    :param body: CreateUserSchema: Validate the request body
    :return: A admin-user object
    """
    try:
        return await UserService.create_admin(body)
    except IntegrityError:
        raise
    except RequestValidationError:
        raise


@user_router.patch("/{user_id}")
async def update_user_by_id(
        user_id: UUID,
        body: UpdateUserRequestSchema,
        current_user: User = Depends(AuthService.get_current_user_from_token),
):
    """
    The update_user_by_id function is used to update a user's information.

    :param user_id: UUID: Specify the user_id of the user to be deleted
    :param body: UpdateUserRequestSchema: Validate the request body
    :param current_user: User: Check if the user is authorized to update another user
    :return: A 201 status code and a message saying that the user was successfully updated
    """
    updated_params = body.model_dump(exclude_unset=True)
    if updated_params == {}:
        raise EmptyParametersException()
    user = await UserService.get_user(user_id)
    if not await UserPermissionsService.check_user_permissions(
            target_user=user, current_user=current_user
    ):
        raise PermissionDeniedException()
    try:
        await UserService.update_user(updated_params, user.id)
    except IntegrityError:
        raise
    except RequestValidationError:
        raise
    else:
        return UJSONResponse(
            content={"detail": "User successfully updated"}, status_code=201
        )


@user_router.delete("/{user_id}")
async def delete_user_by_id(
        user_id: UUID, current_user: User = Depends(AuthService.get_current_user_from_token)
):
    """
    The delete_user_by_id function deletes a user by id.

    :param user_id: UUID: Get the user id from the request
    :param current_user: User: Get the user from the token
    :return: The deleted user
    """
    user_for_deletion = await UserService.get_user(user_id)
    if user_for_deletion is None:
        raise ObjectNotFoundException(detail=f"User with id - {user_id} is not found.")
    if not await UserPermissionsService.check_user_permissions(
            target_user=user_for_deletion, current_user=current_user
    ):
        raise PermissionDeniedException()
    deleted_user = await UserService.deleting_user(user_for_deletion)
    if deleted_user is None:
        return UJSONResponse(
            content={"detail": "User was deleted successfully."}, status_code=200
        )


some_changes(5)

#commit 1

def new_feature():
    print("New feature")

#commit 2


#commit 3

def new_func():
    pass