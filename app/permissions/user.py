from app.exceptions.exceptions import PermissionDeniedException
from app.models.user import Roles
from app.models.user import User


class UserPermissionsService:
    @staticmethod
    async def check_user_permissions(target_user: User, current_user: User) -> bool:
        """
        The check_user_permissions function checks if the current user has permission to perform an action on a target user.
        The function returns True if the current_user is allowed to perform an action on the target_user,
        and raises a PermissionDeniedException otherwise.
        The following rules are used:
            - The current_user can always act upon themselves (target == self)
            - An admin can always act upon any other user (admin role)

        :param target_user: User: Get the user that is being targeted
        :param current_user: User: Pass in the user object of the current user
        :return: True if the target_user is the same as current_user, or if
        """
        if target_user.id == current_user.id:
            return True

        if Roles.ROLE_ADMIN in current_user.roles:
            return True

        if Roles.ROLE_ADMIN in target_user.roles:
            raise PermissionDeniedException()
        return False
