from app.models.todo_list import ToDoList
from app.models.user import User


class TodoListPermissionsService:
    @staticmethod
    async def check_user_permissions(todo_list: ToDoList, current_user: User) -> bool:
        """
        The check_user_permissions function checks if the user is allowed to access a ToDoList.
        :param todo_list: ToDoList: Pass in the todo list that we want to check permissions for
        :param current_user: User: Check if the user is the owner of a todo list
        :return: True if the user is the owner of a todo list, and false otherwise
        """
        if todo_list.user_id == current_user.id:
            return True
        return False
