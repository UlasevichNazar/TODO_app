from fastapi import FastAPI
from fastapi import Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import UJSONResponse
from sqlalchemy.exc import IntegrityError

from app.exceptions.exceptions import AutorizingException
from app.exceptions.exceptions import EmptyParametersException
from app.exceptions.exceptions import ObjectNotFoundException
from app.exceptions.exceptions import PermissionDeniedException


def add_exception_handlers(app: FastAPI):
    """
    The add_exception_handlers function adds exception handlers to the FastAPI app.

    :param app: FastAPI: Pass the fastapi object to the function
    :return: Nothing, it just adds exception handlers to the app
    """
    @app.exception_handler(ObjectNotFoundException)
    def objects_not_found_handler(
        request: Request, exc: ObjectNotFoundException
    ) -> UJSONResponse:
        """
        The objects_not_found_handler function is a custom exception handler that returns a JSON response
        with the error code and detail message from an ObjectNotFoundException. This function is registered
        as an exception handler for the ObjectNotFoundException class in the app's startup() function.

        :param request: Request: Get the request object
        :param exc: ObjectNotFoundException: Pass the exception to the handler
        :return: A UJSONResponse object with the detail and
        error_code attributes of the ObjectNotFoundException object passed in as an argument
        """
        return UJSONResponse(content={"detail": exc.detail}, status_code=exc.error_code)

    @app.exception_handler(IntegrityError)
    def integrity_handler(request: Request, exc: IntegrityError) -> UJSONResponse:
        """
    The integrity_handler function is a custom exception handler that will be used to handle IntegrityError exceptions.
        It returns a UJSONResponse object with the error message from the database and
        an HTTP status code of 503 (Service Unavailable).
        The reason for using this function is because when we use asyncpg, it does not return any useful information about
        why an integrity constraint was violated.
        This function extracts the error message from PostgreSQL and returns it in our response.

        :param request: Request: Access the request object
        :param exc: IntegrityError: Catch the IntegrityError exception
        :return: A json response with a 503 status code
        """
        return UJSONResponse(
            content={"detail": str(exc.orig).split("DETAIL:  ")[-1]}, status_code=503
        )

    @app.exception_handler(RequestValidationError)
    def validation_error_handler(
        request: Request, exc: RequestValidationError
    ) -> UJSONResponse:
        """
        The validation_error_handler function is a custom error handler that will be called when the
        validation fails. It returns a JSON response with the validation errors.

        :param request: Request: Access the request object
        :param exc: RequestValidationError: Catch the RequestValidationError exception
        :return: A UJSONResponse
        """
        return UJSONResponse(
            content={"detail": exc.errors()[0]["msg"]}, status_code=422
        )

    @app.exception_handler(PermissionDeniedException)
    def permission_handler(
        request: Request, exc: PermissionDeniedException
    ) -> UJSONResponse:
        """
        The permission_handler function is a function that takes in the request and an exception,
        and returns a UJSONResponse. The permission_handler function is used to handle exceptions
        that are raised when permissions are denied.

        :param request: Request: Access the request object
        :param exc: PermissionDeniedException: Catch the exception
        :return: A UJSONResponse object
        """
        return UJSONResponse(content={"detail": exc.detail}, status_code=exc.error_code)

    @app.exception_handler(AutorizingException)
    def autorizing_exception_handler(
        request: Request, exc: AutorizingException
    ) -> UJSONResponse:
        """
        The autorizing_exception_handler function is a custom exception handler that returns a JSON response
        with the error message and status code of an AutorizingException. This function is used in the
        exception_handler decorator to handle any exceptions raised by functions decorated with it.

        :param request: Request: Get the request object
        :param exc: AutorizingException: Catch the exception that is raised in the
        :return: A UJSONResponse with the
        """
        return UJSONResponse(content={"detail": exc.detail}, status_code=exc.error_code)

    @app.exception_handler(EmptyParametersException)
    def empty_parameters_handler(
        request: Request, exc: EmptyParametersException
    ) -> UJSONResponse:
        """
    The empty_parameters_handler function is a custom exception handler that returns a JSON response
        with the error code and detail message from an EmptyParametersException. This function is used to
        handle any EmptyParametersExceptions raised by the API.

        :param request: Request: Get the request object
        :param exc: EmptyParametersException: Pass the exception object to the handler
        :return: A UJSONResponse with a status code of 400 and the detail key set to the value of exc
        """
        return UJSONResponse(content={"detail": exc.detail}, status_code=exc.error_code)
