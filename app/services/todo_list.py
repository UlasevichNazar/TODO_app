from typing import List
from typing import Optional
from uuid import UUID

from sqlalchemy import Result

from app.models.todo_list import ToDoList
from app.models.user import User
from app.repositories.todo_list import TodoListRepository
from app.schemas.todo_list import CreateTodoListSchema
from app.schemas.todo_list import ShowTodoListForCreateSchema
from app.schemas.todo_list import ShowTodoListSchema
from app.schemas.user import ShowUserSchema
from database.database import async_session


class TodoListService:
    @staticmethod
    async def create_new_todo_list(
        body: CreateTodoListSchema, user_data: ShowUserSchema
    ) -> ShowTodoListForCreateSchema:
        """
        The create_new_todo_list function creates a new todo list for the user.

        :param body: CreateTodoListSchema: Pass the data that is being passed in from the request body
        :param user_data: ShowUserSchema: Get the user_id from the token
        :return: A ShowTodoListForCreateSchema
        """
        async with async_session() as session:
            creation_todo = await TodoListService._create_todo_list(
                user_id=user_data.id, values=body
            )
            todo_list = await TodoListRepository(session).create_todo(creation_todo)
            await session.commit()
            return ShowTodoListForCreateSchema(
                id=todo_list.id,
                name=todo_list.name,
                description=todo_list.description,
                user=user_data,
                created_at=todo_list.created_at,
                updated_at=todo_list.updated_at,
            )

    @staticmethod
    async def _create_todo_list(
        values: CreateTodoListSchema, user_id: UUID
    ) -> ToDoList:
        """
        The _create_todo_list function is a helper function that creates a new ToDoList object.
        It takes in the values from the CreateTodoListSchema and adds the user_id to it, then uses those values to create
        a new ToDoList object. It returns this newly created list.

        :param values: CreateTodoListSchema: Validate the data that is passed into the function
        :param user_id: UUID: Specify the user_id of the list
        :return: A todolist object
        """
        values_for_create = values.model_dump()
        values_for_create["user_id"] = user_id
        new_list = ToDoList(**values_for_create)
        return new_list

    @staticmethod
    async def get_all_todo_lists(user: User) -> List[ShowTodoListSchema]:
        """
        The get_all_todo_lists function returns a list of all todo lists for the user.

        :param user: User: Get the user id from the user object
        :return: A list of ShowTodoListSchema objects
        """
        async with async_session() as session:
            lists_repo = TodoListRepository(session)
            user_lists = await lists_repo.get_all_user_lists(user.id)
            return [user_list for user_list in user_lists]

    @staticmethod
    async def get_todo_list(list_id: UUID) -> Optional[ToDoList]:
        """
        The get_todo_list function returns a ToDoList object with the given id.

        :param list_id: UUID: Specify the type of data that is expected to be passed into the function
        :return: A todolist object, or none if no list is found
        """
        async with async_session() as session:
            list_repo = TodoListRepository(session)
            list = await list_repo.get_list_by_id(list_id)
            return list

    @staticmethod
    async def update_list(
        updated_params: dict, todo_list: ToDoList
    ) -> Result[ToDoList]:
        """
        The update_list function updates a ToDoList object in the database.

        :param updated_params: dict: Pass in the updated parameters
        :param todo_list: ToDoList: Pass the todo list object
        :return: A result[todolist]
        """
        async with async_session() as session:
            updating_list = TodoListRepository(session)
            updated_todo_list = await updating_list.update_list_by_user(
                todo_list, **updated_params
            )
            await session.commit()
            return updated_todo_list

    @staticmethod
    async def deleting_todo_list(todo_list: ToDoList) -> None:
        """
        The deleting_todo_list function is used to delete a ToDoList object from the database.

        :param todo_list: ToDoList: Pass in the todo_list object that is being deleted
        :return: None
        """
        async with async_session() as session:
            deleting_list = TodoListRepository(session)
            deleting_list_obj = await deleting_list.deleting_todo(todo_list=todo_list)
            await session.commit()
            return deleting_list_obj


def delete():
    pass