from datetime import datetime
from datetime import timedelta
from typing import Optional
from typing import Union

from fastapi import Depends
from fastapi.security import OAuth2PasswordBearer
from fastapi.security import OAuth2PasswordRequestForm
from jose import jwt
from jose import JWTError

from app.exceptions.exceptions import AutorizingException
from app.models.user import User
from app.services.user import UserService
from app.utils.password_hasher import PasswordService
from config.config import setting

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="api/v1/login/token")


class AuthService:
    @staticmethod
    async def _authenticate_user(username: str, password: str) -> Union[User, None]:
        """
        The _authenticate_user function is a helper function that takes in a username and password,
        and returns the user if they exist and their password matches. If either of these conditions are not met,
        it will return None.

        :param username: str: Get the username from the request body
        :param password: str: Verify the password entered by the user
        :return: A user object if the username and password are correct,
        """
        user = await UserService.get_user_by_username_for_auth(username=username)
        if user is None:
            return None
        if not PasswordService.verify_password(password, user.password):
            return None
        return user

    @staticmethod
    async def authenticate_user(form_data: OAuth2PasswordRequestForm):
        """
        The authenticate_user function is responsible for authenticating a user.
        It takes in the form data from the request and uses it to find a user in our database.
        If no user is found, an exception will be raised with details about why authentication failed.
        Otherwise, we generate a token for that user and return it.

        :param form_data: OAuth2PasswordRequestForm: Get the username and password from the request body
        :return: A token
        """
        user = await AuthService._authenticate_user(
            username=form_data.username,
            password=form_data.password,
        )
        if not user:
            raise AutorizingException(detail="Incorrect username or password")
        return await AuthService.generate_token(user)

    @staticmethod
    async def generate_token(user: User):
        """
        The generate_token function generates a new access token for the user.

        :param user: User: Pass the user object to the function
        :return: A token that is valid for 15 minutes
        """
        access_token_expire = timedelta(minutes=setting.ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = await AuthService.create_access_token(
            data={"sub": user.username}, expires_delta=access_token_expire
        )
        return access_token

    @staticmethod
    async def get_current_user_from_token(token: str = Depends(oauth2_scheme)):
        """
        The get_current_user_from_token function is a dependency that will be used in the
            get_current_user function. It takes a token as an argument and returns the user object
            associated with that token. If no user is found, it raises an AutorizingException.

        :param token: str: Get the token from the request header
        :return: A user object if the token is valid
        """
        try:
            payload = jwt.decode(
                token, setting.SECRET_KEY, algorithms=[setting.ALGORITHM]
            )
            username: str = payload.get("sub")
            if username is None:
                raise AutorizingException()
        except JWTError:
            raise AutorizingException()
        user = await UserService.get_user_by_username_for_auth(username=username)
        if user is None:
            raise AutorizingException()
        return user

    @staticmethod
    async def create_access_token(
        data: dict, expires_delta: Optional[timedelta] = None
    ):
        """
        The create_access_token function creates a JWT token with the given data.

        :param data: dict: Pass the user's id to the function
        :param expires_delta: Optional[timedelta]: Set the expiration time of the token
        :return: A jwt token
        """
        to_encode = data.copy()
        if expires_delta:
            expire = datetime.utcnow() + expires_delta
        else:
            expire = datetime.utcnow() + timedelta(
                minutes=setting.ACCESS_TOKEN_EXPIRE_MINUTES
            )

        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(to_encode, setting.SECRET_KEY, setting.ALGORITHM)
        return encoded_jwt
