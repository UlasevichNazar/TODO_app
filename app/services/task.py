from typing import List
from typing import Optional
from uuid import UUID

from sqlalchemy import Result

from app.models.task import Task
from app.repositories.task import TaskRepository
from app.schemas.task import CreateTaskSchema
from app.schemas.task import ShowTaskSchema
from app.schemas.todo_list import ShowTodoListSchema
from database.database import async_session


class TaskService:
    @staticmethod
    async def create_new_task(body: CreateTaskSchema) -> Task:
        """
        The create_new_task function creates a new task in the database.
            Args:
                body (CreateTaskSchema): The schema for creating a new task.

        :param body: CreateTaskSchema: Validate the request body against a schema
        :return: A task object
        """
        async with async_session() as session:
            new_task = TaskRepository(session)
            task = await new_task.create_new_task(body)
            await session.commit()
            return task

    @staticmethod
    async def get_all_tasks(
        todo_lists: List[ShowTodoListSchema],
    ) -> List[ShowTaskSchema]:
        """
        The get_all_tasks function is a helper function that returns all tasks for the user.
            It takes in a list of todo lists and returns a list of tasks.

        :param todo_lists: List[ShowTodoListSchema]: Pass in the list of todo lists that are associated with a user
        :param : Get the id of each todo list that is passed in
        :return: A list of ShowTaskSchema objects
        """
        async with async_session() as session:
            tasks_repo = TaskRepository(session)
            todo_list_ids = tuple(user_list.id for user_list in todo_lists)
            list_of_tasks = await tasks_repo.get_tasks(todo_list_ids)
            return [list_of_task for list_of_task in list_of_tasks]

    @staticmethod
    async def get_task(
        todo_lists: List[ShowTodoListSchema], task_id: UUID
    ) -> Optional[Task]:
        """
        The get_task function is used to retrieve a task from the database.

        :param todo_lists: List[ShowTodoListSchema]: Get the todo_list ids
        :param task_id: UUID: Specify the task id to get
        :return: A task object if the task exists and belongs to
        """
        async with async_session() as session:
            tasks_repo = TaskRepository(session)
            todo_list_ids = tuple(user_list.id for user_list in todo_lists)
            task = await tasks_repo.get_user_task(task_id, todo_list_ids)
            return task

    @staticmethod
    async def update_user_task(task: Task, updated_params: dict) -> Result[Task]:
        """
        The update_user_task function updates a task in the database.
            Args:
                task (Task): The Task object to be updated.
                updated_params (dict): A dictionary of parameters that will be used to update the Task object.

        :param task: Task: Identify the task that needs to be updated
        :param updated_params: dict: Update the task
        :return: A result[task] type
        """
        async with async_session() as session:
            updating_task = TaskRepository(session)
            updated_task = await updating_task.updating_task_by_user(
                task, **updated_params
            )
            await session.commit()
            return updated_task

    @staticmethod
    async def deleting_task(task: Task) -> None:
        """
        The deleting_task function is used to delete a task from the database.
            Args:
                task (Task): The Task object that will be deleted from the database.

        :param task: Task: Pass the task object to the deleting_task function
        :return: The id of the deleted task
        """
        async with async_session() as session:
            task_repo = TaskRepository(session)
            deleting_task_id = await task_repo.deleting_task_by_user(task)
            await session.commit()
            return deleting_task_id
