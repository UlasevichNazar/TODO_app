from typing import List
from typing import Optional
from typing import Union
from uuid import UUID

from sqlalchemy import Result

from app.models.user import Roles
from app.models.user import User
from app.repositories.user import UserRepository
from app.schemas.user import CreateUserSchema
from app.utils.password_hasher import PasswordService
from database.database import async_session


class UserService:
    @staticmethod
    async def get_user(user_id: UUID) -> Optional[User]:
        """
        The get_user function is used to retrieve a user from the database.

        :param user_id: UUID: Specify the user_id that is passed to the function
        :return: A user object if the user exists, or none otherwise
        """
        async with async_session() as session:
            getting_user = UserRepository(session)
            user = await getting_user.get_user_by_id(user_id)
            if user is not None:
                return user

    @staticmethod
    async def get_all_users(user: User) -> Union[List[User], Optional[User]]:
        """
        The get_all_users function returns a list of all users in the database.
        If the user is not an admin, it will return only that user's information.

        :param user: User: Check if the user is an admin
        :return: A list of users if the user has the admin role, and a single user otherwise
        """
        async with async_session() as session:
            user_repo = UserRepository(session)
            if Roles.ROLE_ADMIN in user.roles:
                users = await user_repo.get_all_users()
            else:
                users = await user_repo.get_user_by_id(user.id)
            return users

    @staticmethod
    async def get_user_by_username_for_auth(username: str):
        """
        The get_user_by_username_for_auth function is used to get a user by their username.
        This function is used for authentication purposes, and returns the user's password hash as well.

        :param username: str: Get the username of the user that is currently logged in
        :return: A user object
        """
        async with async_session() as session:
            user = UserRepository(session)
            return await user.get_user_by_username(username=username)

    @staticmethod
    async def create_new_user(body: CreateUserSchema) -> User:
        """
        The create_new_user function creates a new user in the database.

        :param body: CreateUserSchema: Validate the request body
        :return: A user object
        """
        async with async_session() as session:
            user = await UserService._create_user(values=body)
            creation_user = await UserRepository(session).create_user(user)
            await session.commit()
            return creation_user

    @staticmethod
    async def _create_user(values: CreateUserSchema) -> User:
        """
        The _create_user function is a helper function that takes in the validated
        CreateUserSchema and returns a User object. It does this by first dumping the
        validated schema into a dictionary, then hashing the password using our PasswordService,
        and finally creating an instance of User with all of these values.

        :param values: CreateUserSchema: Validate the request body
        :return: A user object
        """
        values_for_create = values.model_dump()
        values_for_create["password"] = PasswordService.get_password_hash(
            values.password
        )
        new_user = User(**values_for_create)
        return new_user

    @staticmethod
    async def create_admin(body: CreateUserSchema) -> User:
        """
        The create_admin function creates a new admin user.

        :param body: CreateUserSchema: Validate the input data
        :return: A user object
        """
        async with async_session() as session:
            user = await UserService._create_admin_user(values=body)
            creation_user = await UserRepository(session).create_user(user)
            await session.commit()
            return creation_user

    @staticmethod
    async def _create_admin_user(values: CreateUserSchema) -> User:
        """
        The _create_admin_user function is a helper function that creates an admin user.
        It takes in the values from the CreateUserSchema and uses them to create a new User object.
        The password is hashed using PasswordService before being stored in the database.

        :param values: CreateUserSchema: Validate the data that is passed in
        :return: A user object
        """
        values_for_create = values.model_dump()
        values_for_create["roles"] = Roles.ROLE_ADMIN
        values_for_create["password"] = PasswordService.get_password_hash(
            values.password
        )
        new_user = User(**values_for_create)
        return new_user

    @staticmethod
    async def update_user(updated_params: dict, user_id: UUID) -> Result[User]:
        """
        The update_user function updates a user's information in the database.

        :param updated_params: dict: Pass in the dictionary of parameters that are to be updated
        :param user_id: UUID: Identify the user to be updated
        :return: A result object
        """
        async with async_session() as session:
            updating_user = UserRepository(session)
            user = await updating_user.get_user_by_id(user_id=user_id)
            updated_user = await updating_user.updated_user(user, **updated_params)
            await session.commit()
            return updated_user

    @staticmethod
    async def deleting_user(user: User) -> None:
        """
        The deleting_user function deletes a user from the database.


        :param user: User: Pass the user object to the function
        :return: An object of the deleted user
        :doc-author: Trelent
        """
        async with async_session() as session:
            deleting_user = UserRepository(session)
            deleting_user_obj = await deleting_user.delete_user(user=user)
            await session.commit()
            return deleting_user_obj
async def new_func():
    pass



async def test():
    print(1)


async def test111():
    pass

